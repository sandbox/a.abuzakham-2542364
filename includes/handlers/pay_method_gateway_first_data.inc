<?php

/**
 * @file
 */
class first_data_pay_method_gateway extends pay_method_gateway {
  var $fd_ssl_keyid = '';
  var $fd_ssl_hmackey = '';
  var $fd_ssl_username = '';
  var $fd_ssl_exactid = '';
  var $fd_ssl_password = '';

  function gateway_url() {
    return 'https://api.globalgatewaye4.firstdata.com/transaction/v12';
  }

  function gateway_request() {
    // Set the transaction type based on requested activity.
    if (!$txntype = $this->first_data_trxtype($this->activity->activity)) {
      drupal_set_message(t("Payment activity '{$this->activity->activity}' is unsupported. Not processing transaction", 'error'));
      return FALSE;
    }

    $req = $this->gateway_request_values($txntype);

    return $req;
  }

  function execute($activity) {
    if ($request = $this->gateway_request()) {
      $ret = drupal_http_request($this->gateway_url(), array('headers' => $this->gateway_headers(), 'method' => 'POST', 'data' => drupal_json_encode($request)));

      $respons = drupal_json_decode($ret->data);

      if ($respons['transaction_approved'] != 1) {
        watchdog('payment', "Gateway Error: @err Payment NOT processed.", array('@err' => $ret->error));
        drupal_set_message(t('We were unable to process your credit card payment. Please verify your card details and try again. If the problem persists, contact us to complete your order.'), 'error');
        $this->activity->data = (array) $ret;
        $this->activity->result = FALSE;
      }
      else {
      	drupal_set_message(t('Transaction processed successfully. Your receipt will be emailed to you. Thank You'));
        $this->activity->result = TRUE;
      }

      // Return TRUE or FALSE on success/failure.
      return ($this->activity->result == 1);
    }
  }

  /**
   * HTTP headers that should be sent to the gateway provider.
   *
   * For example, if your gateway provider requires XML:
   *
   *  function gateway_headers() {
   *     return array('Content-Type' => 'text/xml');
   *  }
   */
  function gateway_headers() {
  	$params = $this->gateway_request();

  	// Prepare the JSON string.
  	$request_content = drupal_json_encode($params);
  	$request_content_type = 'application/json';

  	// Add content headers.
  	$request_headers = array(
  		'Content-Type' => $request_content_type,
  		'Content-Length' => strlen($request_content),
  	);

  	// Add security headers.
  	$request_headers += $this->generateSecurityRequestHeaders($request_headers['Content-Type'], $request_content, 'POST');

  	return $request_headers;
  }



  function generateSecurityRequestHeaders($content_type, $content, $request_method = 'POST') {

  	if (empty($request_method)) {
  		$request_method = 'POST';
  	}
  	else {
  		$request_method = strtoupper($request_method);
  	}

  	// Extract request path.
  	$request_url = $this->gateway_url();
  	$request_url_path = parse_url($request_url, PHP_URL_PATH);

  	// Calculate digest.
  	$content_digest = sha1($content);

  	// Calculate ISO 8601 date - custom since gmdate('c') adds +00:00 instead of Z.
  	$hash_date = gmdate('Y-m-dTH:i:s') . 'Z';
  	$hash_date = str_replace('GMT', 'T', $hash_date);

  	// Calculate Authentication HMAC.
  	$hash_data_parts = array($request_method, $content_type, $content_digest, $hash_date, $request_url_path);
    $hash_data = implode("\n", $hash_data_parts);
  	
  	$hmac = base64_encode(hash_hmac('sha1', $hash_data, $this->fd_ssl_hmackey, TRUE));

  	return array(
  		'X-GGE4-CONTENT-SHA1' => $content_digest,
  		'X-GGE4-Date' =>  $hash_date,
  		'Authorization' => 'GGE4_API ' . $this->fd_ssl_keyid . ':' . $hmac,
  	);
  }

  function settings_form(&$form, &$form_state) {
    parent::settings_form($form, $form_state);
    $group = $this->handler();

    $form[$group]['an']['#type'] = 'fieldset';
    $form[$group]['an']['#collapsible'] = FALSE;
    $form[$group]['an']['#title'] = t('First Data Gateway settings');
    $form[$group]['an']['#group'] = $group;

    $form[$group]['an']['fd_ssl_keyid'] = array(
      '#type' => 'textfield',
      '#title' => t('Key ID'),
      '#description' => t('located in the Terminal Settings Page'),
      '#default_value' => $this->fd_ssl_keyid,
      '#required' => TRUE,
      '#parents' => array($group, 'fd_ssl_keyid'),
    );
    $form[$group]['an']['fd_ssl_hmackey'] = array(
      '#type' => 'textfield',
      '#title' => t('HMAC Key'),
      '#description' => t('located in the Terminal Settings Page'),
      '#default_value' => $this->fd_ssl_hmackey,
      '#required' => TRUE,
      '#parents' => array($group, 'fd_ssl_hmackey'),
    );
    $form[$group]['an']['fd_ssl_username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#description' => t('located in the Terminal Settings Page'),
      '#default_value' => $this->fd_ssl_username,
      '#required' => TRUE,
      '#parents' => array($group, 'fd_ssl_username'),
    );
    $form[$group]['an']['fd_ssl_exactid'] = array(
      '#type' => 'textfield',
      '#title' => t('Gateway ID'),
      '#description' => t('located in the Terminal Settings Page'),
      '#default_value' => $this->fd_ssl_exactid,
      '#required' => TRUE,
      '#parents' => array($group, 'fd_ssl_exactid'),
    );
    $form[$group]['an']['fd_ssl_password'] = array(
      '#type' => 'textfield',
      '#title' => t('Gateway Password'),
      '#description' => t('located in the Terminal Settings Page'),
      '#default_value' => $this->fd_ssl_password,
      '#required' => TRUE,
      '#parents' => array($group, 'fd_ssl_password'),
    );
  }

  function first_data_trxtype($activity) {
    $trxtypes = array(
      'complete'  => '00',
      'authorize' => '01',
    );
    return $trxtypes[$activity];
  }

  function gateway_request_values($txntype) {
  	// Add transaction type.
    $params = array(
     	'transaction_type' => $txntype,
    );

    //Add amount and currency code.
    $params += array(
     	'amount' => $this->total() ? $this->total() : 0,
     	'currency_code' => 'USD',
    );

    if (!empty($this->billto)) {
      // cc_verification_str1: "Street Address|Zip/Postal|City|State/Prov|Country".
      $billing_address_verify_parts['STREET'] = isset($this->billto['street1']) ? $this->billto['street1'] : '';
      $billing_address_verify_parts['CITY'] = isset($this->billto['city']) ? $this->billto['city'] : '';
      $billing_address_verify_parts['STATE'] = isset($this->billto['state']) ? $this->billto['state'] : '';
      $billing_address_verify_parts['COUNTRYCODE'] = isset($this->billto['country']) ? $this->billto['country'] : NULL;
      $billing_address_verify_parts['PHONENUM'] = isset($this->billto['phone']) ? $this->billto['phone'] : NULL;


      $params['cc_verification_str1'] = implode('|', $billing_address_verify_parts);
      $params['cc_verification_str1'] = substr($params['cc_verification_str1'], 0, 41);
    }

    // Add expiration.
    $params += array(
     	'cc_expiry' => $this->cc_expiration(),
    );

    // Add cardholder name.
    $cardholder_name = '';
    if (!empty($this->first_name) && !empty($this->last_name)) {
      // Set to name on card
      $cardholder_name = $this->first_name . ' ' . $this->last_name;
    }

    $params += array(
    	'cardholder_name' => $cardholder_name,
    );

    // Add additional card data.
    $params += array(
     	'cc_number' => substr($this->cc_number, 0, 16),
    );

    // CVV code should only be available during checkout or new cards.
    if (!empty($this->cc_number)) {
      $params['cc_verification_str2'] = substr($this->cc_number, 0, 4);
      $params['cvd_presence_ind'] = "1";
    }

    $params['PartialRedemption'] = "0";

    // Add additional card data.
    $params += array(
    	'cvd_code' => $this->cc_ccv2,
    );

    // Add additional card data.
    if (!empty($this->billto['zip'])) {
    	$params['ZipCode'] = $this->billto['zip'];
    }

    // Add client ip.
    $params['client_ip'] = substr(ip_address(), 0, 15);

    // Add gateway id and password.
    $params += array(
      'gateway_id' => $this->fd_ssl_exactid,
      'password' => $this->fd_ssl_password,
    );

     // Add language.
    $params['language'] = "en";

    // Add client email.
    if (isset($this->mail)) {
      $params['client_email'] = substr($this->mail, 0, 255);
    }

    // TODO.
    $params['track1'] = '';
    $params['track2'] = '';
    $params['authorization_num'] = '';

    return $params;
  }
}
